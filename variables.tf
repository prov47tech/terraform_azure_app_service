variable "resource_group_name" {
  description = "The name of the resource group"
}
#
variable "location" {
  description = "The location/region where the resources are created."
}
#
variable "app_service_plan_id" {
  description = "The app service plan id"
}
#
variable "app_service_name" {
  description = "The app service plan name"
}
#
variable "dotnet_framework_version" {
  description = "The dotnet framework version"
}
#
variable "scm_type" {
  description = "The source control type"
}
#
variable "http2_enabled" {
  description = "The http2 setting"
}
#
variable "always_on" {
  description = "The always on setting"
}
#
variable "https_only" {
  description = "The always on setting"
}
#
variable "db_name" {
  description = "The connect string database name"
}
#
variable "db_type" {
  description = "The connect string database type"
}
#
variable "connection_string" {
  description = "The sql connect string"
}
#
variable "ftps_state" {
  description = "Disables FTPS uploads"
}
#
variable "websockets_enabled" {
  description = "Set websocket settings"
}
