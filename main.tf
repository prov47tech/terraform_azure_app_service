#
resource "azurerm_app_service" "app_service" {
  name                = "${var.app_service_name}"
  location            = "${var.location}"
  resource_group_name = "${var.resource_group_name}"
  app_service_plan_id = "${var.app_service_plan_id}"
  https_only = "${var.https_only}"
#
  site_config {
    dotnet_framework_version = "${var.dotnet_framework_version}"
    scm_type = "${var.scm_type}"
    http2_enabled = "${var.http2_enabled}"
    always_on = "${var.always_on}"
    ftps_state = "${var.ftps_state}"
    websockets_enabled = "${var.websockets_enabled}"
  }
#
  connection_string {
    name  = "${var.db_name}"
    type  = "${var.db_type}"
    value = "${var.connection_string}"
  }
}
