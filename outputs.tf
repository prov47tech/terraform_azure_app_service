# 
output "app_service_name" {
  description = "The app service server name"
  value = "${azurerm_app_service.app_service.name}"
}

